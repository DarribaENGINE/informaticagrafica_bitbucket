#include "Materials.h"



Materials::Materials() {
}


Materials::~Materials() {
}


void Materials::setAmbient(glm::vec3 ambient) {
	mAmbient = ambient;
}

void Materials::setDiffuse(glm::vec3 diffuse) {
	mDiffuse = diffuse;
}

void Materials::setSpecular(glm::vec3 specular) {
	mSpecular = specular;
}

void Materials::setShininess(float shininess) {
	mShininess = shininess;
}

glm::vec3 Materials::getAmbient() {
	return mAmbient;
}

glm::vec3 Materials::getDiffuse() {
	return mDiffuse;
}

glm::vec3 Materials::getSpecular() {
	return mSpecular;
}

float Materials::getShininess() {
	return mShininess; 
}

#pragma once
//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#include <glm/glm.hpp>			//OpenGL Mathematics 

struct Position {
	GLfloat  x;
	GLfloat  y;
	GLfloat  z;
};

struct ColorRGBA8 {
	ColorRGBA8() :r(0), g(0), b(0), a(0) {	}
	ColorRGBA8(GLubyte R, GLubyte G, GLubyte B, GLubyte A) : r(R), g(G), b(B), a(A)  {}
	GLubyte r;
	GLubyte g;
	GLubyte b;
	GLubyte a;
};


struct UV
{
	GLfloat u;
	GLfloat v;
};

struct Vertex {
	Position position;
	Position normals;
	ColorRGBA8 color;
	UV uv;
	
	void setPosition(GLfloat  x, GLfloat  y, GLfloat  z) {
		position.x = x;
		position.y = y;
		position.z = z;
	}

	void setUV(GLfloat x, GLfloat y) {
		uv.u = x;
		uv.v = y;
	}
	void setNormal(GLfloat  x, GLfloat  y, GLfloat  z) {
		normals.x = x;
		normals.y = y;
		normals.z = z;
	}
	
	void setColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
		color.r = r;
		color.g = g;
		color.b = b;
		color.a = a;
	}
};

//struct UV struct normales
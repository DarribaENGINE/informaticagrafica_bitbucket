#pragma once
#include <glm\glm.hpp>
class Sphere {
private:
	glm::vec3 sPosition;
	float sRadius;
public:
	Sphere();
	~Sphere();
	
	void setRadius(float radius);

	void setPosition(glm::vec3 position);
	void setPosition(float x, float y, float z);

	float getRadius();
	glm::vec3 getPosition();

	bool checkCollision(Sphere enemy);
};


#pragma once


//Third-party libraries
#include <GL/glew.h>			//The OpenGL Extension Wrangler
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include "Window.h"
#include "GLSLProgram.h"
#include "FPSLimiter.h"
#include "OpenGLBuffers.h"
#include "Vertex.h"
#include "Geometry.h"
#include "InputManager.h"
#include "Camera.h"
#include "Sphere.h"
#include "TextureManager.h"

#include "Materials.h"
#include "Light.h"

#define PLAYER 0

//#define ENEMY 1
//#define ITEM 6
#define CAR_1 7
#define CAR_2 8
#define CAR_3 9
#define CAR_4 10
#define CAR_5 11
#define CAR_6 12
#define CAR_7 13
#define CAR_8 14


#define PLAYER_COLLISION 15
#define CAR_1_COLLISION 16
#define CAR_2_COLLISION 17
#define CAR_3_COLLISION 18
#define CAR_4_COLLISION 19
#define CAR_5_COLLISION 20
#define CAR_6_COLLISION 21
#define CAR_7_COLLISION 22
#define CAR_8_COLLISION 23



//Game has four possible states: INIT (Preparing environment), PLAY (Playing), EXIT (Exit from the game) or MENU (Game menu)
enum class GameState{INIT, PLAY, EXIT, MENU};

//This class manages the game execution
class Game {
	public:						
		Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS);	//Constructor
		~Game();					//Destructor
		void run();					//Game execution

	private:
			//Attributes	
		std::string _windowTitle;		//Window Title
		int _screenWidth;				//Screen width in pixels				
		int _screenHeight;				//Screen height in pixels				
		GameState _gameState;			//It describes the game state				
		Window _window;					//Manage the OpenGL context
		GLSLProgram _colorProgram;		//Manage the shader programs
		FPSLimiter _fpsLimiter;			//Manage the synchronization between frame rate and refresh rate
		OpenGLBuffers _openGLBuffers;	//Manage the openGL buffers
		Geometry _gameElements;			//Manage the game elements
		InputManager _inputManager;		//Manage the input devices
		Camera _camera;

		TextureManager tManager;
		Sphere player;
		Sphere enemy;

		int hunger = 50;

		int textureDrawMode = 1;

		Materials material;
		Light light;

		GLuint modelMatrixUniform;
		GLuint viewMatrixUniform;
		GLuint projectionMatrixUniform;
		GLuint _drawModeUniform;

		GLuint _newColorUniform;
		GLuint _textureDataLocation;
		GLuint _textureScaleFactorLocation;

		GLuint rockTexture;
		GLuint waterTexture;
		GLuint lavaTexture;

		GLuint gModelNormalMatrix;
		GLuint gMaterialAmbientUniform;
		GLuint gMaterialDiffuseUniform;
		GLuint gMaterialSpecularUniform;
		GLuint gMaterialShininessUniform;

		GLuint gLightAmbientUniform;
		GLuint gLightDiffuseUniform;
		GLuint gLightSpecularUniform;
		GLuint gLightPositionUniform;
		GLuint gLightDirectionUniform;

		GLuint gLightLinearUniform;
		GLuint gLightConstantUniform;
		GLuint gLightQuadraticUniform;
		GLuint gLightPowerUniform;
		GLuint gLightCutoffUniform;

		GLuint gLightTypeUniform;

		GLuint gLightEnableUniform;
		GLuint gViewerPositionUniform;

			//Internal methods
		void initSystems();
		void initShaders();		
		void gameLoop();
		void processInput();
		void doPhysics();
		void executePlayerCommands();
		void renderGame();			
};


#pragma once
#pragma once
#include <glm\glm.hpp>
#include <GL/glew.h>
#include <glm/glm.hpp>			//OpenGL Mathematics 
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
	float _aspectRatio;
	glm::mat4 _projectionMatrix;
	glm::mat4 _viewMatrix;
	float _FOV;
	float _far;
	float _near;
	float _projectionWidth;
	float _projectionHeight;
	glm::vec3 _cameraPos;
	glm::vec3 _cameraFront;
	glm::vec3 _cameraUp;

public:
	Camera();
	~Camera();

	void computeProjection(bool perspectiveType);
	void setViewMatrix();
	glm::mat4& getViewMatrix();
	glm::mat4& getProjectionMatrix();
};


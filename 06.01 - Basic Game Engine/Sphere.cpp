#include "Sphere.h"



Sphere::Sphere()
{
}


Sphere::~Sphere()
{
}


void Sphere::setRadius(float radius) {
	sRadius = radius;
}

void Sphere::setPosition(glm::vec3 position) {
	sPosition = position;
}
void Sphere::setPosition(float x, float y, float z) {
	sPosition.x = x;
	sPosition.y = y;
	sPosition.z = z;
}

float Sphere::getRadius() {
	return sRadius;
}

glm::vec3 Sphere::getPosition() {
	return sPosition;
}


// Collision


bool Sphere::checkCollision(Sphere enemy) {
	float deltaXSquared = enemy.getPosition().x - sPosition.x;
	float deltaYSquared = enemy.getPosition().y - sPosition.y;

	deltaXSquared *= deltaXSquared;
	deltaYSquared *= deltaYSquared;

	float sumRadisquared = sRadius + enemy.getRadius();

	sumRadisquared *= sumRadisquared;

	if (deltaXSquared + deltaYSquared <= sumRadisquared) {
		return true;
	} 
	return false;
}
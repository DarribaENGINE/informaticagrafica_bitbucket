#include "Camera.h"



Camera::Camera() {
	_near = 0.1;
	_far = 30.0;
	_projectionWidth = 30.0;
	_projectionHeight = 30.0;
	_FOV = 45.0;
	_cameraPos = glm::vec3(10.0, 0.0, 0.0);
	_cameraFront = glm::vec3(0.0, 0.0, 0.0);
	_cameraUp = glm::vec3(0.0, 0.0, 0.0);
	_aspectRatio = 1;

}


Camera::~Camera()
{
}

void Camera::computeProjection(bool perspectiveType) {
	//True = Ortho
	//False = Perspective
	if (perspectiveType == true) {
		_projectionMatrix = glm::ortho(-_projectionWidth / 2, _projectionWidth / 2,
			-_projectionHeight / 2, _projectionHeight / 2, _near, _far);
		_cameraPos = glm::vec3(1.0,1.0,3.0);
		_cameraFront = glm::vec3(1.0,2.0,-3.0);
	}
	else {
		_projectionMatrix = glm::perspective(_FOV, _aspectRatio, _near, _far);
		_cameraPos = glm::vec3(0.0, -2.0, 2.0);
		_cameraFront = glm::vec3(0.0, 0.0, 0.0);
		// Falta calibrar.
	}
}

void Camera::setViewMatrix() {
	glm::vec3 cameraDirection = glm::normalize(_cameraPos - _cameraFront);
	glm::vec3 up = glm::vec3(0.0f, 0.0f, 1.0f);
	glm::vec3 cameraRight = glm::normalize(glm::cross(up, cameraDirection));
	_cameraUp = glm::cross(cameraDirection, cameraRight);
	_viewMatrix = glm::lookAt(_cameraPos, _cameraFront, _cameraUp);
}

glm::mat4& Camera::getViewMatrix() {
	return _viewMatrix;
}

glm::mat4& Camera::getProjectionMatrix() {
	return _projectionMatrix;
}

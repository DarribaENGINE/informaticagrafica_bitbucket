#include "Game.h"


/**
* Constructor
* Note: It uses an initialization list to set the parameters
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, bool enableLimiterFPS, int maxFPS, bool printFPS) :
	_windowTitle(windowTitle), 
	_screenWidth(screenWidth), 
	_screenHeight(screenHeight),
	_gameState(GameState::INIT), 
	_fpsLimiter(enableLimiterFPS, maxFPS, printFPS),
	tManager() {


}

/**
* Destructor
*/
Game::~Game() {

}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
		//Start the game if all the elements are ready
	//Title
	cout << "**************************" << endl;
	cout << "*         ANGRY ROCK         *" << endl;
	cout << "******************************" << endl;
	cout << "CONTROLS:" << endl;
	cout << "	A: Move left" << endl;
	cout << "	D: Move right" << endl;
	cout << "******************************" << endl;
	cout << "	O: Camera othorgraphic mode" << endl;
	cout << "	P: Camera perspective mode" << endl;
	cout << "	L: Change light mode" << endl;

	gameLoop();
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
	//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);
	//Compile and Link shader
	initShaders();
	//Set up the openGL buffers
	_openGLBuffers.initializeBuffers(_colorProgram);
	//Load the current scenario
	_gameElements.loadGameElements("./resources/scene2D.txt");

	// init collisions
	player.setRadius(0.5*0.1);
	enemy.setRadius(0.5*0.1);


	//Set perspective camera
	//True = Ortho
	//False = Perspective
	_camera.computeProjection(false);
	_camera.setViewMatrix();

	material.setAmbient(glm::vec3(0.25, 0.0725, 0.0725));
	material.setDiffuse(glm::vec3(0.780392, 0.568627, 0.113725));
	material.setSpecular(glm::vec3(0.296648, 0.296648, 0.296648));
	material.setShininess(512);

	light.ambient = glm::vec3(1.0f, 0.8f, 0.8f);
	light.diffuse = glm::vec3(0.64902, 0.654902, 0.654902);
	light.specular = glm::vec3(1.0f, 0.843137f, 0.0f);
	light.position = glm::vec3(0.0f, 0.0f, 2.0f);
	light.direction = glm::vec3(0.0f, 0.0f, -1.0f);
	light.power = 5.0f;
	light.linear = 2.8f;
	light.quadratic = 5.4f;
	light.constant = 1.0f;
	light.cutoff = glm::cos(glm::radians(45.0f));
	light.type = 0;
}

/*
* Initialize the shaders:
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::initShaders() {
		//Compile the shaders
	_colorProgram.addShader(GL_VERTEX_SHADER, "./resources/shaders/vertex-shader.txt");
	_colorProgram.addShader(GL_FRAGMENT_SHADER, "./resources/shaders/fragment-shader.txt");
	_colorProgram.compileShaders();
		//Attributes must be added before linking the code
	_colorProgram.addAttribute("vertexPosition");
	_colorProgram.addAttribute("vertexUV");
	_colorProgram.addAttribute("vertexNormal");
		//Link the compiled shaders
	_colorProgram.linkShaders();
		//Bind the uniform variables. You must enable shaders before gettting the uniforme variable location
	_colorProgram.use();
	modelMatrixUniform = _colorProgram.getUniformLocation("modelMatrix");
	viewMatrixUniform = _colorProgram.getUniformLocation("viewMatrix");
	projectionMatrixUniform = _colorProgram.getUniformLocation("projectionMatrix");
	
	// Draw Mode, texures
	_drawModeUniform = _colorProgram.getUniformLocation("drawMode");
	_newColorUniform = _colorProgram.getUniformLocation("objectColor");
	_textureDataLocation = _colorProgram.getUniformLocation("textureData");
	_textureScaleFactorLocation = _colorProgram.getUniformLocation("textureScaleFactor");
	
	gModelNormalMatrix = _colorProgram.getUniformLocation("modelNormalMatrix");

	// Material
	// set up the uniform material variables
	gMaterialAmbientUniform = _colorProgram.getUniformLocation("material.ambient");
	gMaterialDiffuseUniform = _colorProgram.getUniformLocation("material.diffuse");
	gMaterialSpecularUniform = _colorProgram.getUniformLocation("material.specular");
	gMaterialShininessUniform = _colorProgram.getUniformLocation("material.shininess");
	
	// LIGHT
	gLightAmbientUniform = _colorProgram.getUniformLocation("light.ambient");
	gLightDiffuseUniform = _colorProgram.getUniformLocation("light.diffuse");
	gLightSpecularUniform = _colorProgram.getUniformLocation("light.specular");
	gLightPositionUniform = _colorProgram.getUniformLocation("light.position");
	gLightDirectionUniform = _colorProgram.getUniformLocation("light.direction");

	gLightLinearUniform = _colorProgram.getUniformLocation("light.linear");
	gLightConstantUniform = _colorProgram.getUniformLocation("light.constant");
	gLightQuadraticUniform = _colorProgram.getUniformLocation("light.quadratic");
	gLightPowerUniform = _colorProgram.getUniformLocation("light.power");
	gLightCutoffUniform = _colorProgram.getUniformLocation("light.cutoff");

	gLightTypeUniform = _colorProgram.getUniformLocation("light.type");
	
	gLightEnableUniform = _colorProgram.getUniformLocation("lightingEnabled");

	gViewerPositionUniform = _colorProgram.getUniformLocation("viewerPosition");

	_colorProgram.unuse();

	// Init Textures
	rockTexture = tManager.getTextureID("./resources/textures/Rock.png");
	waterTexture = tManager.getTextureID("./resources/textures/water.jpg");
	lavaTexture = tManager.getTextureID("./resourceS/textures/lava.png");
}

/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	while (_gameState != GameState::EXIT) {		
			//Start synchronization between refresh rate and frame rate
		_fpsLimiter.startSynchronization();
			//Process the input information (keyboard and mouse)
		processInput();
			//Execute the player actions (keyboard and mouse)
		executePlayerCommands();
			//Update the game status
		doPhysics();
			//Draw the objects on the screen
renderGame();
//Force synchronization
_fpsLimiter.forceSynchronization();
	}
}

/*
* Processes input with SDL
*/
void Game::processInput() {
	_inputManager.update();
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;
	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}

}


/**
* Executes the actions sent by the user by means of the keyboard and mouse
*/
void Game::executePlayerCommands() {
	if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)) {
		glm::ivec2 mouseCoords = _inputManager.getMouseCoords();
		cout << mouseCoords.x << ", " << mouseCoords.y << endl;
	}

	/*if (_inputManager.isKeyDown(SDLK_w)) {
		//y++
		(_gameElements.getGameElement(PLAYER))._translate.y = (_gameElements.getGameElement(PLAYER))._translate.y + 0.025f;

	}*/

	if (_inputManager.isKeyDown(SDLK_a)) {
		//x--
		if (_gameElements.getGameElement(PLAYER)._translate.x >= -0.9) {
			(_gameElements.getGameElement(PLAYER))._translate.x = (_gameElements.getGameElement(PLAYER))._translate.x - 0.025f;
		}
	}
	/*if (_inputManager.isKeyDown(SDLK_s)) {
		//y--
		(_gameElements.getGameElement(PLAYER))._translate.y = (_gameElements.getGameElement(PLAYER))._translate.y - 0.025f;
	}*/

	if (_inputManager.isKeyDown(SDLK_d)) {
		// x++
		if (_gameElements.getGameElement(PLAYER)._translate.x <=0.9) {
			(_gameElements.getGameElement(PLAYER))._translate.x = (_gameElements.getGameElement(PLAYER))._translate.x + 0.025f;
		}
	}

	if (_inputManager.isKeyPressed(SDLK_q)) {
		textureDrawMode = (textureDrawMode + 1) % 3;
		cout << "draw mode: " << textureDrawMode << endl;
	}	
	if (_inputManager.isKeyPressed(SDLK_l)) {
		light.type = (light.type + 1) % 3;
		cout << "light.type: " << light.type << endl;
	}

	if (_inputManager.isKeyPressed(SDLK_o)) {
		_camera.computeProjection(true);
		_camera.setViewMatrix();
		printf("\n Camera mode = Orthographic");
	}

	if (_inputManager.isKeyPressed(SDLK_p)) {
		_camera.computeProjection(false);
		_camera.setViewMatrix();
		printf("\n Camera mode = Perspective");
	}


}

/*
* Update the game objects based on the physics
*/
void Game::doPhysics() {
	//(_gameElements.getGameElement(ENEMY))._angle = (_gameElements.getGameElement(ENEMY))._angle + 5;
	//(_gameElements.getGameElement(ENEMY))._rotation.z = 1.0f;
	//(_gameElements.getGameElement(ENEMY))._rotation.y = 1.0f;

	//(_gameElements.getGameElement(ITEM))._angle = (_gameElements.getGameElement(ITEM))._angle + 5;
	//(_gameElements.getGameElement(ITEM))._rotation.y = 1.0f;

	// Cada 120 ticks te baja el hambre = Sea temp. 
	static int counter = 0;
	if (counter >= 120) {
		counter = 0;
		hunger +=10;
		cout << "Sea temperature: "<< hunger << endl;
	} 
	counter++;
	


	// Se actualiza la posicion de la esfera del jugador
	player.setPosition(_gameElements.getGameElement(PLAYER)._translate);

	for (int i = 3; i < _gameElements.getNumGameElements(); i++) { //Traslacion
		_gameElements.getGameElement(i)._translate.y = (_gameElements.getGameElement(i))._translate.y - 0.05;
		if (_gameElements.getGameElement(i)._translate.y < -5) {
			_gameElements.getGameElement(i)._translate.y = rand() % 10 + 5;
		}

		// Check collision with enemies
		enemy.setPosition(_gameElements.getGameElement(i)._translate);

		if (player.checkCollision(enemy)) {
			_gameElements.getGameElement(i)._translate.y = -5;
			hunger -= 5;
			cout << "Sea temeperature: " << hunger << endl;

		}
	}

	if (hunger == 100) {
		hunger = 50;
		_gameElements.getGameElement(PLAYER)._translate = glm::vec3(0, 0, 0);
		for (int i = 3; i < _gameElements.getNumGameElements(); i++) { //Traslacion
			_gameElements.getGameElement(i)._translate.y = -5;
		}
		cout << "The sea is boiling! GAME OVER... Restarting..." << endl;
	}
}
	
	//(_gameElements.getGameElement(CAR_1))._translate.y = (_gameElements.getGameElement(CAR_1))._translate.y + 0.1;


/**
* Draw the sprites on the screen
*/
void Game::renderGame() {
		//Temporal variable
	GameObject currentRenderedGameElement;

		//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//Bind the GLSL program. Only one code GLSL can be used at the same time
	_colorProgram.use();

	//**************CAMERA*********************

	glUniformMatrix4fv(viewMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera.getViewMatrix()));
	glUniformMatrix4fv(projectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(_camera.getProjectionMatrix()));

	//**************CAMERA*********************/

	glUniform1i(gLightEnableUniform, 1);
	// Pass the camera position
	glUniform3fv(gViewerPositionUniform, 1, glm::value_ptr(glm::vec3(10.0, 0.0, 0.0)));
	// Activate textures
	glActiveTexture(GL_TEXTURE0);
	glUniform1i(_drawModeUniform, 1);

	glUniform4fv(_newColorUniform, 1, glm::value_ptr(glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)));
	glUniform1i(_textureDataLocation, 0);

	// Send Lights
	glUniform3fv(gLightAmbientUniform, 1, glm::value_ptr(light.ambient));
	glUniform3fv(gLightDiffuseUniform, 1, glm::value_ptr(light.diffuse));
	glUniform3fv(gLightSpecularUniform, 1, glm::value_ptr(light.specular));

	glUniform3fv(gLightDirectionUniform, 1, glm::value_ptr(light.direction));
	glUniform3fv(gLightPositionUniform, 1, glm::value_ptr(light.position));
	glUniform1i(gLightTypeUniform, light.type);

	glUniform1f(gLightPowerUniform, light.power);
	glUniform1f(gLightLinearUniform, light.linear);
	glUniform1f(gLightConstantUniform, light.constant);
	glUniform1f(gLightQuadraticUniform, light.quadratic);
	glUniform1f(gLightCutoffUniform, light.cutoff);


	// Send Material
	glUniform3fv(gMaterialAmbientUniform, 1, glm::value_ptr(material.getAmbient()));
	glUniform3fv(gMaterialDiffuseUniform, 1, glm::value_ptr(material.getDiffuse()));
	glUniform3fv(gMaterialSpecularUniform, 1, glm::value_ptr(material.getSpecular()));
	glUniform1f(gMaterialShininessUniform, material.getShininess());

		//For each one of the elements: Each object MUST BE RENDERED based on its position, rotation and scale data
	for (int i = 0; i < _gameElements.getNumGameElements(); i++) {		
		glm::mat3 normalMatrix;
		currentRenderedGameElement = _gameElements.getGameElement(i);
				//Each object MUST BE RENDERED based on the position, 
				//rotation and scale for each one of the GAME ELEMENTS

				//Create the transformation matrix (translate*rotate*scale)
				//remember to inverse matrices in the inverse order.
		if (currentRenderedGameElement._objectType == 3) {
			glBindTexture(GL_TEXTURE_2D, rockTexture);
			glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		}
		// WATER
		if (currentRenderedGameElement._objectType == 4) {
			glBindTexture(GL_TEXTURE_2D, waterTexture);
			glUniform2f(_textureScaleFactorLocation, 1.0f, 40.0f);
		}
		// WALL
		if (currentRenderedGameElement._objectType == 2) {
			glBindTexture(GL_TEXTURE_2D, rockTexture);
			glUniform2f(_textureScaleFactorLocation, 1.0f, 40.0f);
		}
		// ENEMIES
		if (currentRenderedGameElement._objectType == 1) {
			glBindTexture(GL_TEXTURE_2D, lavaTexture);
			glUniform2f(_textureScaleFactorLocation, 1.0f, 1.0f);
		}

		glm::mat4 modelMatrix; //Identity matrix
		modelMatrix = glm::translate(modelMatrix, currentRenderedGameElement._translate);
		if (currentRenderedGameElement._angle != 0) {
			modelMatrix = glm::rotate(modelMatrix, glm::radians(currentRenderedGameElement._angle), currentRenderedGameElement._rotation);
		}
		
		modelMatrix = glm::scale(modelMatrix, currentRenderedGameElement._scale);
		normalMatrix = glm::mat3(glm::transpose(glm::inverse(modelMatrix)));
		//Pass the model matrix		
		glUniformMatrix4fv(modelMatrixUniform, 1, GL_FALSE, glm::value_ptr(modelMatrix));
		glUniformMatrix3fv(gModelNormalMatrix, 1, GL_FALSE, glm::value_ptr(normalMatrix));

		//Send data to GPU
		_openGLBuffers.sendDataToGPU(_gameElements.getData(currentRenderedGameElement._objectType), _gameElements.getNumVertices(currentRenderedGameElement._objectType));	
		
		
		// unbind textures
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	//Unbind the program
	_colorProgram.unuse();
	//****************************************************************************
	//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}



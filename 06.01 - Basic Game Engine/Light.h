#pragma once
#include <glm\glm.hpp>
class Light {
public:
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	glm::vec3 position;
	glm::vec3 direction;

	float linear;
	float constant;
	float quadratic;

	float cutoff;
	float power;

	int type;
	Light();
	~Light();
};


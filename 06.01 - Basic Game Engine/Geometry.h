#pragma once
#include "Vertex.h"
#include <vector>
#include "GameObject.h"
#include "ObjectLoader.h"

//Cubes
#define BLUE_CUBE 0
#define RED_CUBE 1
#define WHITE_CUBE 2
//Pyramid
#define RED_PYRAMID 3
#define NUMBASICOBJECTS 5

#define WATER 4

//This class stores and manipulates all the objects loaded from the text file
class Geometry
{
	std::vector<Vertex*> _verticesData;//[NUMBASICOBJECTS]; //vector
	std::vector<int> _numVertices;//[NUMBASICOBJECTS]; //vector

	ObjectLoader _objectLoader;

	std::vector <GameObject> _listOfObjects;
	

public:
	Geometry();
	~Geometry();
	void loadGameElements(char fileName[100]);
	Vertex * getData(int objectID);
	int getNumVertices(int objectID);
	int getNumGameElements();
	GameObject & getGameElement(int objectID);



	void loadCube(int CUBE, glm::vec4 color);
	void loadPyramid(int PYRAMID, glm::vec4 color);
	void loadBasicObject();
	
};


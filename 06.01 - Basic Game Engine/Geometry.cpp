#include "Geometry.h"
#include "ErrorManagement.h"
#include <iostream>
#include <fstream>
#include "ObjectLoader.h"

using namespace std;


/*
* Constructor 
*/
Geometry::Geometry()
{
	_numVertices.resize(NUMBASICOBJECTS);
	_verticesData.resize(NUMBASICOBJECTS);
	
	loadCube(BLUE_CUBE, glm::vec4(0, 0, 255, 255));
	loadCube(RED_CUBE, glm::vec4(255, 0, 0,255));
	loadCube(WHITE_CUBE, glm::vec4(255, 255, 255, 255));
	loadCube(WATER, glm::vec4(255,0, 255, 255));
	loadPyramid(RED_PYRAMID, glm::vec4(255, 255, 0, 255));
	loadBasicObject();


}




void Geometry::loadCube(int CUBE, glm::vec4 color) {
	_numVertices[CUBE] = 36;
	_verticesData[CUBE] = new Vertex[_numVertices[CUBE]];
	
	//FRONT
	_verticesData[CUBE][0].setPosition(-0.5f, -0.5f, -0.5f); // Bien
	_verticesData[CUBE][0].setUV(0, 0); // Bien
	_verticesData[CUBE][0].setNormal(0, -1, 0); // Bien

	_verticesData[CUBE][1].setPosition(0.5f, -0.5f, -0.5f); // Bien
	_verticesData[CUBE][1].setUV(1, 0); // Bien
	_verticesData[CUBE][1].setNormal(0, -1, 0); // Bien

	_verticesData[CUBE][2].setPosition(0.5f, -0.5f, 0.5f); // Bien
	_verticesData[CUBE][2].setUV(1, 1); // Bien
	_verticesData[CUBE][2].setNormal(0, -1, 0); // Bien

	_verticesData[CUBE][3].setPosition(0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][3].setUV(1, 1); // Bien
	_verticesData[CUBE][3].setNormal(0, -1, 0); // Bien
	
	_verticesData[CUBE][4].setPosition(-0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][4].setUV(0, 1); // Bien
	_verticesData[CUBE][4].setNormal(0, -1, 0); // Bien

	_verticesData[CUBE][5].setPosition(-0.5f, -0.5f, -0.5f); //Bien
	_verticesData[CUBE][5].setUV(0, 0); // Bien
	_verticesData[CUBE][5].setNormal(0, -1, 0); // Bien
														
	//RIGHT
	_verticesData[CUBE][6].setPosition(0.5f, -0.5f, -0.5f); //Bien
	_verticesData[CUBE][6].setUV(0, 0); // Bien
	_verticesData[CUBE][6].setNormal(1, 0, 0); // Bien

	_verticesData[CUBE][7].setPosition(0.5f, 0.5f, -0.5f);//Bien
	_verticesData[CUBE][7].setUV(1, 0); // Bien
	_verticesData[CUBE][7].setNormal(1, 0, 0); // Bien

	_verticesData[CUBE][8].setPosition(0.5f, 0.5f, 0.5f); //Bien
	_verticesData[CUBE][8].setUV(1, 1); // Bien
	_verticesData[CUBE][8].setNormal(1, 0, 0); // Bien
	
	_verticesData[CUBE][9].setPosition(0.5f, 0.5f, 0.5f);//Bien
	_verticesData[CUBE][9].setUV(1, 1); // Bien
	_verticesData[CUBE][9].setNormal(1, 0, 0); // Bien

	_verticesData[CUBE][10].setPosition(0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][10].setUV(0, 1); // Bien
	_verticesData[CUBE][10].setNormal(1, 0, 0); // Bien

	_verticesData[CUBE][11].setPosition(0.5f, -0.5f, -0.5f); //Bien 
	_verticesData[CUBE][11].setUV(0, 0); // Bien
	_verticesData[CUBE][11].setNormal(1, 0, 0); // Bien

	//BACK
	_verticesData[CUBE][12].setPosition(0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][12].setUV(0, 0); // Bien
	_verticesData[CUBE][12].setNormal(0, 1, 0); // Bien

	_verticesData[CUBE][13].setPosition(-0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][13].setUV(1, 0); // Bien
	_verticesData[CUBE][13].setNormal(0, 1, 0); // Bien

	_verticesData[CUBE][14].setPosition(-0.5f, 0.5f, 0.5f); //Bien
	_verticesData[CUBE][14].setUV(1, 1);
	_verticesData[CUBE][14].setNormal(0, 1, 0);

	_verticesData[CUBE][15].setPosition(-0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][15].setUV(1, 1);
	_verticesData[CUBE][15].setNormal(0, 1, 0);
	//
	_verticesData[CUBE][16].setPosition(-0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][16].setUV(0, 1);
	_verticesData[CUBE][16].setNormal(0, 1, 0);

	_verticesData[CUBE][17].setPosition(0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][17].setUV(0, 0);
	_verticesData[CUBE][17].setNormal(0, 1, 0);

	//LEFT
	_verticesData[CUBE][18].setPosition(-0.5f, 0.5f, -0.5f); //Bien
	_verticesData[CUBE][18].setUV(0, 0);
	_verticesData[CUBE][18].setNormal(-1,0,0);

	_verticesData[CUBE][19].setPosition(-0.5f, -0.5f, -0.5f); //Bien
	_verticesData[CUBE][19].setUV(0, 1);
	_verticesData[CUBE][19].setNormal(-1, 0, 0);

	_verticesData[CUBE][20].setPosition(-0.5f, -0.5f, 0.5f); //Bien	
	_verticesData[CUBE][20].setUV(1, 1);
	_verticesData[CUBE][20].setNormal(-1, 0, 0);

	_verticesData[CUBE][21].setPosition(-0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][21].setUV(1, 1);
	_verticesData[CUBE][21].setNormal(-1, 0, 0);

	_verticesData[CUBE][22].setPosition(-0.5f, 0.5f, 0.5f); //Bien
	_verticesData[CUBE][22].setUV(0, 1);
	_verticesData[CUBE][22].setNormal(-1, 0, 0);

	_verticesData[CUBE][23].setPosition(-0.5f, 0.5f, -0.5f); //
	_verticesData[CUBE][23].setUV(0,0);
	_verticesData[CUBE][23].setNormal(-1, 0, 0);
	
	//TOP
	_verticesData[CUBE][24].setPosition(-0.5f, -0.5f, 0.5f); // Bien
	_verticesData[CUBE][24].setUV(0, 0);
	_verticesData[CUBE][24].setNormal(0, 0, 1);

	_verticesData[CUBE][25].setPosition(0.5f, -0.5f, 0.5f); // Bien
	_verticesData[CUBE][25].setUV(1, 0);
	_verticesData[CUBE][25].setNormal(0, 0, 1);

	_verticesData[CUBE][26].setPosition(0.5f, 0.5f, 0.5f); // Bien
	_verticesData[CUBE][26].setUV(1, 1);
	_verticesData[CUBE][26].setNormal(0, 0, 1);

	_verticesData[CUBE][27].setPosition(0.5f, 0.5f, 0.5f); //Bien
	_verticesData[CUBE][27].setUV(1, 1);
	_verticesData[CUBE][27].setNormal(0, 0, 1);

	_verticesData[CUBE][28].setPosition(-0.5f, 0.5f, 0.5f); //Bien
	_verticesData[CUBE][28].setUV(0, 1);
	_verticesData[CUBE][28].setNormal(0, 0, 1);

	_verticesData[CUBE][29].setPosition(-0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][29].setUV(0, 0);
	_verticesData[CUBE][29].setNormal(0, 0, 1);

	//DOWN
	_verticesData[CUBE][30].setPosition(-0.5f, -0.5f, -0.5f); // Bien
	_verticesData[CUBE][30].setUV(0, 0);
	_verticesData[CUBE][30].setNormal(0, -1, 0);

	_verticesData[CUBE][31].setPosition(-0.5f, -0.5f, 0.5f); // Bien
	_verticesData[CUBE][31].setUV(0, 1);
	_verticesData[CUBE][31].setNormal(0, -1, 0);

	_verticesData[CUBE][32].setPosition(0.5f, -0.5f, -0.5f); // Bien
	_verticesData[CUBE][32].setUV(1, 1);
	_verticesData[CUBE][32].setNormal(0, -1, 0);

	_verticesData[CUBE][33].setPosition(0.5f, -0.5f, -0.5f); //Bien
	_verticesData[CUBE][33].setUV(1, 1);
	_verticesData[CUBE][33].setNormal(0, -1, 0);
	
	_verticesData[CUBE][34].setPosition(0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][34].setUV(0, 1);
	_verticesData[CUBE][34].setNormal(0, -1, 0);
	
	_verticesData[CUBE][35].setPosition(-0.5f, -0.5f, 0.5f); //Bien
	_verticesData[CUBE][35].setUV(0, 0);
	_verticesData[CUBE][35].setNormal(0, -1, 0);
	
}

void Geometry::loadPyramid(int PYRAMID, glm::vec4 color) {
	_numVertices[PYRAMID] = 18;
	_verticesData[PYRAMID] = new Vertex[_numVertices[PYRAMID]];

	//Tr 1
	_verticesData[PYRAMID][0].setPosition(-0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][0].setUV(0, 0);
	_verticesData[PYRAMID][0].setNormal(0, -1, 0);

	_verticesData[PYRAMID][1].setPosition(0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][1].setUV(1, 0);
	_verticesData[PYRAMID][1].setNormal(0, -1, 0);

	_verticesData[PYRAMID][2].setPosition(0.0f, 0.0f, 0.5f);
	_verticesData[PYRAMID][2].setUV(0.5, 1);
	_verticesData[PYRAMID][2].setNormal(0, -1, 0);

	//Tr 2
	_verticesData[PYRAMID][3].setPosition(0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][3].setUV(0, 0);
	_verticesData[PYRAMID][3].setNormal(1, 0, 0);

	_verticesData[PYRAMID][4].setPosition(0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][4].setUV(1, 0);
	_verticesData[PYRAMID][4].setNormal(1, 0, 0);

	_verticesData[PYRAMID][5].setPosition(0.0f, 0.0f, 0.5f);
	_verticesData[PYRAMID][5].setUV(0.5, 1);
	_verticesData[PYRAMID][5].setNormal(1, 0, 0);
	////Tr 3
	_verticesData[PYRAMID][6].setPosition(0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][6].setUV(0, 0);
	_verticesData[PYRAMID][6].setNormal(1, 0, 0);

	_verticesData[PYRAMID][7].setPosition(-0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][7].setUV(1, 0);
	_verticesData[PYRAMID][7].setNormal(1, 0, 0);

	_verticesData[PYRAMID][8].setPosition(0.0f, 0.0f, 0.5f);
	_verticesData[PYRAMID][8].setUV(0.5, 1);
	_verticesData[PYRAMID][8].setNormal(1, 0, 0);
	//Tr 4
	_verticesData[PYRAMID][9].setPosition(-0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][9].setUV(0, 0);
	_verticesData[PYRAMID][9].setNormal(1, 0, 0);

	_verticesData[PYRAMID][10].setPosition(-0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][10].setUV(1, 0);
	_verticesData[PYRAMID][10].setNormal(1, 0, 0);

	_verticesData[PYRAMID][11].setPosition(0.0f, 0.0f, 0.5f);
	_verticesData[PYRAMID][11].setUV(0.5, 1);
	_verticesData[PYRAMID][11].setNormal(1, 0, 0);
	//Tr DOWN
	_verticesData[PYRAMID][12].setPosition(-0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][12].setUV(0, 0);
	_verticesData[PYRAMID][12].setNormal(0, 0, -1);

	_verticesData[PYRAMID][13].setPosition(-0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][13].setUV(1, 0);
	_verticesData[PYRAMID][13].setNormal(0, 0, -1);

	_verticesData[PYRAMID][14].setPosition(0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][14].setUV(0, 1);
	_verticesData[PYRAMID][14].setNormal(0, 0, -1);

	_verticesData[PYRAMID][15].setPosition(0.5f, 0.5f, -0.5f);
	_verticesData[PYRAMID][15].setUV(1, 1);
	_verticesData[PYRAMID][15].setNormal(0, 0, -1);

	_verticesData[PYRAMID][16].setPosition(0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][16].setUV(1,1);
	_verticesData[PYRAMID][16].setNormal(0, 0, -1);

	_verticesData[PYRAMID][17].setPosition(-0.5f, -0.5f, -0.5f);
	_verticesData[PYRAMID][17].setUV(0, 1);
	_verticesData[PYRAMID][17].setNormal(0, 0, -1);

	for (int i = 0;i < _numVertices[PYRAMID];i++) {
		//_verticesData[CUBE][i].setColor(255,255,255,255);
	}
}

void Geometry::loadBasicObject() {
	


}



/*
* Load the game elements from a text file
*/
void Geometry::loadGameElements(char fileName[100]){	
	/* Text format
	<number of game elements>
	<type of game element> <vec3 position> <angle> <vec3 rotation> <vec3 scale>	
	*/
	int numGameElements;
	GameObject tempObject;
	glm::vec3 vector3fElements;
	ifstream file;
	file.open(fileName);
	 
	if (file.is_open()){
		//TODO: Read the content and add it into the data structure
		file >> numGameElements;
		for (int i = 0;i < numGameElements;i++) {
			//<type of game element>
			file >> tempObject._objectType;

			//translate - <vec3 position>
			file >> tempObject._translate.x;
			file >> tempObject._translate.y;
			file >> tempObject._translate.z;

			//<angle>
			file >> tempObject._angle;

			//<vec3 rotation>
			file >> tempObject._rotation.x;
			file >> tempObject._rotation.y;
			file >> tempObject._rotation.z;

			//<vec3 scale>
			file >> tempObject._scale.x;
			file >> tempObject._scale.y;
			file >> tempObject._scale.z;

			_listOfObjects.push_back(tempObject);
		}
		file.close();
	}
	else{
		string message = "The file "+ string(fileName)+" doesn't exists";
		ErrorManagement::errorRunTime(message);
	}

}

Geometry::~Geometry() {
	for (int i = 0; i < NUMBASICOBJECTS; i++) {
		delete _verticesData[i];
	}
}


/*
* Get the vertices data for an specific object
* @param objectID is the identifier of the requested object
* @return Vertex * is an array with all the vertices data
*/
Vertex * Geometry::getData(int objectID){
	return _verticesData[objectID];
}

/*
* Get the number of vertices for an specific object
* @param objectID is the identifier of the requested object
* @return int is the number of vertices
*/

int Geometry::getNumVertices(int objectID){
	return _numVertices[objectID];
}

/*
* Get the number of elements to render
*/
int Geometry::getNumGameElements() {
	return _listOfObjects.size();
}

/*
* Get the number of vertices of an specific game element
* @param objectID is the identifier of the requested object
*/
GameObject & Geometry::getGameElement(int objectID) {
	return (_listOfObjects[objectID]);
}



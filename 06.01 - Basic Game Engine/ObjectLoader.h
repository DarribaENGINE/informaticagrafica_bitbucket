#pragma once
#include "ErrorManagement.h"
#include "Vertex.h"
#include "GameObject.h"
#include "parserAse.h"
#include <fstream>
#include <sstream>
#include <iostream>


class ObjectLoader
{
private:
	vector<float> split(const string &line, char delim);
	vector<string> splitString(const string &line, char delim);
public:
	ObjectLoader();
	~ObjectLoader();
	void loadObj(char * fileName, std::vector <int> & numVertices, std::vector <Vertex *> & verticesData);
	void loadAse(char * fileName, std::vector <int> & numVertices, std::vector <Vertex *> & verticesData);
	void computeNormals(File *Object);
};

